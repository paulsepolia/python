class Employee1:
    def __init__(self, name, sal):
        self.name = name
        self.salary = sal

    def get_salary(self):
        return self.salary


class Employee2:
    def __init__(self, name, sal):
        self.name = name
        self._salary = sal  # protected

    def get_salary(self):
        return self._salary

    @property
    def salary(self):
        return self._salary


class Employee3:
    def __init__(self, name, sal):
        self.name = name
        self.__salary = sal  # private

    def get_salary(self):
        return self.__salary


o1 = Employee1("person1", 1000)
print(o1.name)
print(o1.salary)
print(o1.get_salary())
print(o1.__dict__)

o2 = Employee2("person2", 2000)
print(o2.name)
print(o2.salary)
print(o2.get_salary())
print(o2.__dict__)

o3 = Employee3("person3", 3000)
print(o3.name)
# print(o3.__salary)
print(o3.get_salary())
print(o3.__dict__)
