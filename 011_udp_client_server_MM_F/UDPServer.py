import socket


class UPDServer:

    def __init__(self, listening_port, module_loc):
        print("-->> Hello from class --> UPDServer")
        self.server_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.server_socket.bind(('', listening_port))
        self.module_loc = module_loc
        print("-->> The server is ready to receive!")

    def receive(self, module_loc):
        """Connects to a port and receives messages"""
        counter = 0
        while True:
            message, client_address = self.server_socket.recvfrom(2048)
            counter += 1
            if message and counter % module_loc == 0:
                print("-->> the message is = " + message.decode()[0])


class MainExecution:

    def __init__(self, listening_port, module_loc):
        print("-->> Hello from class --> MainExecution")
        self.obj = UPDServer(listening_port, module_loc)

    def main(self):
        UPDServer.receive(self.obj, self.obj.module_loc)
