import random
import string


class UPDClient:

    def __init__(self, server_address, server_port, msg_length, client_socket):
        print("-->> Hello from class --> UPDClient")
        self.server_address = server_address
        self.server_port = server_port
        self.msg_length = msg_length
        self.client_socket = client_socket

    @staticmethod
    def random_string(str_length):
        """Generate a random string of fixed length"""
        letters = string.ascii_lowercase
        return ''.join(random.choice(letters) for _ in range(str_length))

    @staticmethod
    def send(server_address, server_port, msg_length, client_socket):
        """Connects to a port and sends a msg"""
        while True:
            msg = UPDClient.random_string(msg_length)
            client_socket.sendto(msg.encode(), (server_address, server_port))


class MainExecution:

    def __init__(self, server_address, server_port, msg_length, client_socket):
        print("-->> Hello from class --> MainExecution")
        self.server_address = server_address
        self.server_port = server_port
        self.msg_length = msg_length
        self.client_socket = client_socket

    @staticmethod
    def main(obj):
        UPDClient.send(obj.server_address,
                       obj.server_port,
                       obj.msg_length,
                       obj.client_socket)
