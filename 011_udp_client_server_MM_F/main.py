import UDPServer
import UDPClient
import threading
import time
import socket

'''
help function to do the threading easily
'''


def f1():
    UDPServer.MainExecution.main(UDPServer.MainExecution(12000, 100))


def f2():
    client_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    UDPClient.MainExecution.main(UDPClient.MainExecution('127.0.0.1', 12000, 10000, client_socket))


'''
main execution here
'''

print("------------------------------------------>> starting the server...")
thread1 = threading.Thread(target=f1, args=[])
thread1.start()
print("------------------------------------------>> server started!")

time.sleep(2)

print("------------------------------------------>> starting the client...")
thread2 = threading.Thread(target=f2, args=[])
thread2.start()
print("------------------------------------------>> client started!")

thread1.join()
thread2.join()

print("------------------------------------------>> end of program")
