import concurrent.futures
import urllib.request
import itertools


def do_nothing():
    return


def build_some_urls(x1, x2, num_urls):
    urls = []

    for x1 in range(x1, x1 + 1):
        ip1 = "x1.x2.x3.x4"
        ip1 = ip1.replace('x1', str(x1))
        for x2 in range(x2, x2 + 1):
            ip2 = ip1.replace('x2', str(x2))
            for x3 in range(0, 256):
                ip3 = ip2.replace('x3', str(x3))
                for x4 in range(0, 256):
                    ip_final = ip3.replace('x4', str(x4))
                    urls.append(ip_final)
                    if len(urls) == num_urls:
                        return urls


# Retrieve a single page and report the URL and contents
def load_url(url, timeout):
    with urllib.request.urlopen(url, timeout=timeout) as conn:
        return conn.read()


# We can use a with statement to ensure threads are cleaned up promptly
def main():
    for i, j in itertools.product(range(256), range(256)):
        with concurrent.futures.ThreadPoolExecutor(max_workers=20) as executor:

            # Start the load operations and mark each future with its URL
            future_to_url = \
                {executor.submit(load_url, url, 60): url for url in build_some_urls(i, j, 256 * 256)}

            for future in concurrent.futures.as_completed(future_to_url):
                url = future_to_url[future]
                try:
                    data = future.result()
                except Exception as exc:
                    do_nothing()  # print('%r generated an exception: %s' % (url, exc))
                else:
                    print('%r page is %d bytes' % (url, len(data)))


if __name__ == '__main__':
    main()
