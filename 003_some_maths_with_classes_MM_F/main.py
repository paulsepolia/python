class MathsOperations:

    def __init__(self):
        print("--> Hello from class --> MathsOperations")

    @staticmethod
    def addition(a, b):
        return a + b

    @staticmethod
    def multiplication(a, b):
        return a * b

    @staticmethod
    def subtraction(a, b):
        return a - b

    @staticmethod
    def division(a, b):
        return a / b


class MainExecution:
    def __init__(self, a, b):
        self.a = a
        self.b = b
        print("--> Hello from class --> MainExecution")

    @staticmethod
    def main(obj):
        print(MathsOperations.addition(obj.a, obj.b))
        print(MathsOperations.subtraction(obj.a, obj.b))
        print(MathsOperations.multiplication(obj.a, obj.b))
        print(MathsOperations.division(obj.a, obj.b))


# ====================#
# main execution here #
# ====================#

MainExecution.main(MainExecution(10, 12))
