import copy
import time
import threading


class ArraysGame:
    def __init__(self, dim):
        self.dim = dim

    @staticmethod
    def create_list(dim):
        list_loc = []
        for i in range(dim):
            list_loc.append(i)
        return list_loc


def benchmark_fun(dim_loc, do_max):
    for j in range(do_max):
        print("---------------------------------------------------------->> ", j)
        print("-->> thread id -->", threading.current_thread().ident)
        print("-->> building the list...")
        t0 = time.time()
        list1 = ArraysGame.create_list(dim_loc)
        t1 = time.time()
        print("-->> time used to build the list -->", "{0:.10f}".format(t1 - t0))

        print("-->> list1[0] -->", list1[0])
        print("-->> list1[dim_loc - 1] -->", list1[dim_loc - 1])

        print("-->> deep-copying list1 to list2...")
        t0 = time.time()
        list2 = copy.deepcopy(list1)
        t1 = time.time()
        print("-->> time used to deep copy the list1 to list 2 -->", "{0:.10f}".format(t1 - t0))

        print("-->> shallow-copying list2 to another list3...")
        t0 = time.time()
        list3 = copy.copy(list2)
        t1 = time.time()
        print("-->> time used to shallow copy the list2 to list3 -->", "{0:.10f}".format(t1 - t0))

        print("-->> clearing the list1...")
        t0 = time.time()
        list1.clear()
        t1 = time.time()
        print("-->> time used to clear the list1 -->", "{0:.10f}".format(t1 - t0))

        print("-->> clearing the list2...")
        t0 = time.time()
        list2.clear()
        t1 = time.time()
        print("-->> time used to clear the list2 -->", "{0:.10f}".format(t1 - t0))

        print("-->> clearing the list3...")
        t0 = time.time()
        list3.clear()
        t1 = time.time()
        print("-->> time used to clear the list3 -->", "{0:.10f}".format(t1 - t0))


if __name__ == '__main__':
    DO_MAX = 1000000
    DIM_LOC = 10000000

    thread1 = threading.Thread(target=benchmark_fun, args=(DIM_LOC, DO_MAX))
    thread2 = threading.Thread(target=benchmark_fun, args=(DIM_LOC, DO_MAX))
    thread3 = threading.Thread(target=benchmark_fun, args=(DIM_LOC, DO_MAX))
    thread4 = threading.Thread(target=benchmark_fun, args=(DIM_LOC, DO_MAX))

    thread1.start()
    thread2.start()
    thread3.start()
    thread4.start()

    thread1.join()
    thread2.join()
    thread3.join()
    thread4.join()
