#!/bin/python

import re

def append_zeroes_fun(my_str):

    my_str_local = my_str.strip()

    res = re.findall(r'\d+', my_str_local)

    if len(res) > 0:
        val1 = res[0]
    else:
        return "ERROR-1"

    if len(val1) == 1:
        val1 = "000" + val1
    elif len(val1) == 2:
        val1 = "00" + val1
    elif len(val1) == 3:
        val1 = "0" + val1
    elif len(val1) == 4:
        val1 = "" + val1
    else:
        val1 = "ERROR-2"

    my_str_local = my_str_local.replace(str(res[0]), str(val1), 1)

    return my_str_local

