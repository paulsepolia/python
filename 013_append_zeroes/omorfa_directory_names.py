#!/bin/python

from __future__ import print_function
import subprocess
import os
import append_zeroes

# list only the directories

list_of_dirs = filter(os.path.isdir, os.listdir(os.getcwd()))

# create a list and put the names of the dirs

i = 0
while i < len(list_of_dirs):
    list_of_dirs[i] = "_".join(list_of_dirs[i].split())
    i += 1

# eliminate the characters: "#", "(", ")", "[", "]", "-", "'", ","
# by replacing them with nice ones

i = 0
while i < len(list_of_dirs):
    list_of_dirs[i] = list_of_dirs[i].replace("#", "")
    list_of_dirs[i] = list_of_dirs[i].replace("(", "_")
    list_of_dirs[i] = list_of_dirs[i].replace(")", "_")
    list_of_dirs[i] = list_of_dirs[i].replace("[", "_")
    list_of_dirs[i] = list_of_dirs[i].replace("]", "_")
    list_of_dirs[i] = list_of_dirs[i].replace("-", "_")
    list_of_dirs[i] = list_of_dirs[i].replace(",", "_")
    list_of_dirs[i] = list_of_dirs[i].replace("'", "")
    i += 1

# replace "__" with "_"

i = 0
while i < len(list_of_dirs):
    list_of_dirs[i] = list_of_dirs[i].replace("__", "_")
    i += 1

# replace "_" with "" if it is the last character

i = 0
while i < len(list_of_dirs):
    if list_of_dirs[i].endswith('_'):
        list_of_dirs[i] = list_of_dirs[i][:-1]
    i += 1

# append zeroes

i = 0
while i < len(list_of_dirs):
    list_of_dirs[i] = append_zeroes.append_zeroes_fun(list_of_dirs[i])
    i += 1

# save the new list of dirs

list_of_dirs_new = list_of_dirs

# get again the list of dirs

list_of_dirs = filter(os.path.isdir, os.listdir(os.getcwd()))

# rename the directories

i = 0
while i < len(list_of_dirs):
    subprocess.call(["mv", list_of_dirs[i], list_of_dirs_new[i]])
    i += 1

# end
