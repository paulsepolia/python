from socket import *


class TCPClient:

    def __init__(self, server_address, server_port):
        print("-->> Hello from class --> TCPClient")
        self.server_address = server_address
        self.server_port = server_port

    @staticmethod
    def send_and_receive(server_address, server_port):
        while True:
            client_socket = socket(AF_INET, SOCK_STREAM)
            client_socket.connect((server_address, server_port))
            sentence = input('-->> Give your input here: ')
            client_socket.send(sentence.encode())
            modified_sentence = client_socket.recv(1024)
            print('-->> From Server: ' + modified_sentence.decode())
            client_socket.close()


class MainExecution:

    def __init__(self, server_address, server_port):
        print("-->> Hello from class --> MainExecution")
        self.server_address = server_address
        self.server_port = server_port

    @staticmethod
    def main(obj):
        TCPClient.send_and_receive(obj.server_address, obj.server_port)


# ====================#
# main execution here #
# ====================#

MainExecution.main(MainExecution('127.0.0.1', 12000))
