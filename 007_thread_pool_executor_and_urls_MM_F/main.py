import concurrent.futures
import urllib.request

URLS = ['http://www.foxnews.com',
        'http://www.cnn.com',
        'http://www.europe.wsj.com',
        'http://www.bbc.co.uk',
        'http://www.dell.com',
        'http://www.hsbc.co.uk',
        'http://www.hsbc.com',
        'http://www.youtube.com',
        'http://www.gmail.com',
        'http://www.yahoo.com',
        'http://www.hotmail.com',
        'http://www.apple.com',
        'http://www.apple.co.uk',
        'http://www.apple.co.uk',
        'http://www.bbc.co.uk',
        'http://www.facebook.com',
        'http://www.facebook.com',
        'http://www.facebook.com',
        'http://www.facebook.com',
        'http://www.nike.com',
        'http://www.nike.com',
        'http://www.nike.com',
        'http://www.nike.com',
        'http://www.youtube.com',
        'http://www.stackoverflow.com',
        'http://www.foxnews.com',
        'http://www.cnn.com',
        'http://www.europe.wsj.com',
        'http://www.bbc.co.uk',
        'http://www.dell.com',
        'http://www.hsbc.co.uk',
        'http://www.hsbc.com',
        'http://www.youtube.com',
        'http://www.gmail.com',
        'http://www.yahoo.com',
        'http://www.hotmail.com',
        'http://www.apple.com',
        'http://www.apple.co.uk',
        'http://www.apple.co.uk',
        'http://www.bbc.co.uk',
        'http://www.facebook.com',
        'http://www.facebook.com',
        'http://www.facebook.com',
        'http://www.facebook.com',
        'http://www.nike.com',
        'http://www.nike.com',
        'http://www.nike.com',
        'http://www.nike.com',
        'http://www.youtube.com',
        'http://www.stackoverflow.com',
        'http://www.outlook.com',
        'http://www.outlook.com',
        'http://www.outlook.com',
        'http://www.outlook.com',
        'http://www.microsoft.com',
        'http://www.microsoft.com',
        'http://www.microsoft.com',
        'http://www.microsoft.com',
        'http://www.microsoft.com',
        'http://www.intel.com',
        'http://www.intel.com',
        'http://www.intel.com',
        'http://www.intel.com',
        'http://www.intel.com',
        'http://www.visa.com',
        'http://www.visa.com',
        'http://www.visa.com',
        'http://www.visa.com',
        'http://www.visa.com']


# Retrieve a single page and report the URL and contents
def load_url(url, timeout):
    with urllib.request.urlopen(url, timeout=timeout) as conn:
        return conn.read()


# We can use a with statement to ensure threads are cleaned up promptly
def main():
    for idx in range(1000000):
        with concurrent.futures.ThreadPoolExecutor(max_workers=len(URLS)) as executor:

            # Start the load operations and mark each future with its URL
            future_to_url = \
                {executor.submit(load_url, url, 20): url for url in URLS}

            for future in concurrent.futures.as_completed(future_to_url):
                url = future_to_url[future]
                try:
                    data = future.result()
                except Exception as exc:
                    print('%r generated an exception: %s' % (url, exc))
                else:
                    print("---------------------------------------------------->>", idx)
                    print('%r page is %d bytes' % (url, len(data)))


if __name__ == '__main__':
    main()
