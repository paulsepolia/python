import socket


class TCPServer:

    def __init__(self, listening_port):
        print("-->> Hello from class --> TCPServer")
        self.server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.server_socket.bind(('', listening_port))
        self.server_socket.listen(True)

        print("-->> The server is ready to receive!")

    def receive_capitalize_send(self):
        while True:
            connection_socket, addr = self.server_socket.accept()
            sentence = connection_socket.recv(2048)
            capitalized_sentence = sentence.upper()
            connection_socket.send(capitalized_sentence)
            connection_socket.close()
            if capitalized_sentence == '-1':
                break


class MainExecution:

    def __init__(self, listening_port):
        print("-->> Hello from class --> MainExecution")
        self.obj = TCPServer(listening_port)

    def main(self):
        TCPServer.receive_capitalize_send(self.obj)
