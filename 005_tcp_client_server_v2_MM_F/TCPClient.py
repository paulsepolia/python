import random
import string
from socket import *


class TCPClient:

    def __init__(self, server_address, server_port, msg_length, mod_value):
        print("-->> Hello from class --> TCPClient")
        self.server_address = server_address
        self.server_port = server_port
        self.msg_length = msg_length
        self.mod_value = mod_value

    @staticmethod
    def random_string(self, string_length):
        """Generate a random string of fixed length"""
        letters = string.ascii_lowercase
        return ''.join(random.choice(letters) for _ in range(string_length))

    @staticmethod
    def send_and_receive(server_address, server_port, msg_length, mod_value):
        """Connects to a port and sends a msg"""
        counter = 0
        while True:
            counter += 1
            client_socket = socket(AF_INET, SOCK_STREAM)
            client_socket.connect((server_address, server_port))
            sentence = TCPClient.random_string(TCPClient, msg_length)
            client_socket.send(sentence.encode())
            modified_sentence = client_socket.recv(4096)

            if counter % mod_value == 0:
                print('-->> From Server --> ' +
                      modified_sentence.decode()[0] +
                      ' --> ' +
                      str(counter))

            client_socket.close()


class MainExecution:

    def __init__(self, server_address, server_port, msg_length, mod_value):
        print("-->> Hello from class --> MainExecution")
        self.server_address = server_address
        self.server_port = server_port
        self.msg_length = msg_length
        self.mod_value = mod_value

    @staticmethod
    def main(obj):
        TCPClient.send_and_receive(obj.server_address,
                                   obj.server_port,
                                   obj.msg_length,
                                   obj.mod_value)
