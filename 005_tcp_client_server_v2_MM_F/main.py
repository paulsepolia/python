import TCPServer
import TCPClient
import threading
import time

'''
help function to do the threading easily
'''


def f1():
    TCPServer.MainExecution.main(TCPServer.MainExecution(12000))


def f2():
    TCPClient.MainExecution.main(TCPClient.MainExecution('127.0.0.1', 12000, 100000, 100))


'''
main execution here
'''

print("------------------------------------------>> starting the server...")
thread1 = threading.Thread(target=f1, args=[])
thread1.start()
print("------------------------------------------>> server started!")

time.sleep(2)

print("------------------------------------------>> starting the client...")
thread2 = threading.Thread(target=f2, args=[])
thread2.start()
print("------------------------------------------>> client started!")

thread1.join()
thread2.join()

print("------------------------------------------>> end of program")
